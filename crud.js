// CRUD Operations
/*
	C - Create (Insert document/s)
	R - Read/Retrieve (View specific/all document/s)
	U - Update (Edit specific document/s)
	D - Delete (Remove specific document/s)

	- CRUD Operations are the heart of any backend application.
*/

// [SECTION] Insert a documents (Create)

/*
	-Syntax:
		- db.collectionName.insertOne({object});
	Comparison with javascript
		object.object.method({object});
*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact:{
		phone: "87654321",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Phython"],
	department: "none"
});

/*
	Mini Activity

    Scenario: We will create a database that will simulate a hotel database.
    1. Create a new database called "hotel".
    2. Insert a single room in the "rooms" collection with the following details:
        
        name - single
        accommodates - 2
        price - 1000
        description - A simple room with basic necessities
        rooms_available - 10
        isAvailable - false

    3. Use the "db.getCollection('users').find({})" query to check if the document is created.
    
    4. Take a screenshot of the Robo3t result and send it to the batch hangouts.

*/

	// Insert a single document
	db.rooms.insertOne({
		name: "single",
		accommodates: 2,
		price: 1000,
		description: "A simple room with basic necessities",
		rooms_available: 10,
		isAvailable: false
	});

	// Insert many 
	/*
		-Syntax
			- db.collectionName.insertMany([{objectA}, {objectB}]);
	*/

	db.users.insertMany([
		{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact:{
				phone: "87654321",
				email: "stephenhawking@gmail.com"
			},
			courses: ["Phython", "React", "PHP"],
			department: "none"
		},
		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact:{
				phone: "87654321",
				email: "neilarmstrong@gmail.com"
			},
			courses: ["React", "Laravel", "Sass"],
			department: "none"
		}
	]);

	/*
		1. Using the hotel database, insert multiple room in the "rooms" collection with the following details:

			//Room 1:

			name - double
			accomodates - 3
			price - 2000
			description - A room fit for a small family going on a vacation
			rooms_available - 5
			isAvailable - false

			//Room 2:

			name - queen
			accomodates - 4
			price - 4000
			description - A room with a queen sized bed perfect for a simple getaway
			rooms_available - 15
			isAvailable - false

		2. Use the "db.getCollection('users').find({})" query to check if the document is created.
		
		3. Take a screenshot of the Robo3t result and send it to the batch hangouts.
	*/

	db.rooms.insertMany([
		{
			name: "double",
			accommodates: 3,
			price: 2000,
			description: " A room fit for a small family going on a vacation.",
			rooms_available: 5,
			isAvailable: false
		},
		{
			name: "queen",
			accommodates: 4,
			price: 4000,
			description: " A room with a queen sized bed perfect for a simple getaway.",
			rooms_available: 15,
			isAvailable: false
		}
	]);

	// [SECTION] Retrieve a document (Read)

	/*
		-Syntax:
			-db.collectionName.find({}); // get all the documents
			-db.collectionName.find({field:value}); // get a specific document.
	*/

	// Find all the documents in the collection
	db.users.find({});

	// Find a specific document in the collection using the field a value.
	db.users.find({firstName: "Stephen"});
	db.users.find({department: "none"});

	// Find documents with multiple parameters.
	/*
		-Syntax:
			- db.collectionName.find({fieldA:valueA, fieldB:valueB});
	*/

	db.users.find({lastName: "Armstrong", age: 82});

	// [SECTION] Updating documents (Update)

	// Create a document to update
	db.users.insertOne({
		firstName: "Test",
		lastName: "Test",
		age: 0,
		contact: {
			phone: "00000000",
			email: "test@gmail.com"
		},
		course:[],
		department: "none"
	});

	/*
		- Just like the find method, methods that only manipulate a single document will only update the FIRST document that matches the search criteria.
		-Syntax:
		 	- db.collectionName.updateOne({criteria}, {$set: {field:value}});
	*/

	db.users.updateOne(
		{firstName: "Test"},
		{
			$set: {
				firstName: "Bill",
				lastName: "Gates",
				age: 65,
				contact: {
					phone: "12345678",
					email: "bill@gmail.com"
				},
				course: ["PHP", "Laravel", "HTML"],
				department: "Operations",
				status: "active"
			}
		}
	);

	// db.users.updateOne(
	// 	{_id: ObjectId("63081ec3956a234a1efd64fa")},
	// 	{
	// 		$set:{department: "HR"}
	// 	}
	// )

	// Updating multiple documents
	/*
		- Syntax:
		- db.collectionName.updateMany({criteria}, {$set: {field:value}});

	*/

	db.users.updateMany(
		{department: "none"},
		{
			$set:{department: "HR"}
		}
	);

	// Replace One
	/*
		- Can be used if replacing the whole document if necessary.
		-Syntax:
		 - db.collectionName.replaceOne({criteria}, {field: value});

	*/

	db.users.replaceOne(
		{firstName: "Bill"},
		{
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "bill@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations"
		}
	);

	// [SECTION] Removing documents [DELETE]

	// Deleting a single document
	/*
		- Syntax:
			-	db.collectionName.deleteOne({criteria})
	*/

		db.users.deleteOne({
			firstName: "Test"
		});

	// Delete Many
		/*
			- Be careful when using "deleteMany" method. If no search criteria is provided, it will delete all documents in the collection.
			- Syntax:
				- db.collectionName.deleteMany({criteria});
				- db.collectionName.deleteMany({}); // DO NOT USE
		*/

		db.users.deleteMany({
			firstName: "Test"
		});

		db.rooms.deleteMany({
			rooms_available: 0
		});

		db.rooms.find();

		// [SECTION] Advanced queries
		/*
			
		*/

		// Query an embedded document

		db.users.find({
			contact:{
				phone:"87654321",
				email:"stephenhawking@gmail.com"
			}
		});

		// Query on nested field
		db.users.find({
			"contact.email" : "stephenhawking@gmail.com"
		});

		db.users.find({
			courses: ["CSS","JavaScript","Python"]
		});

		// Querying an array disregarding the array elements order.
		db.users.find({
			courses: {$all: ["JavaScript","CSS","Python"]} 
		});

		db.users.insertOne({
			nameArr:[
			{
				nameA: "Juan"
			},
			{
				nameB: "Tamad"
			}]
		});

		db.users.find({
			nameArr:{
				nameA: "Juan"
			}
		})




